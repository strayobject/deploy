<?php

class bitbucketDeploy
{
    protected $method;
    protected $remote_ip;
    protected $allowed_ip;
    protected $mail_to;
    protected $payload;
    protected $working_path;
    protected $remote;
    protected $branch;
    protected $repo_name;
    protected $setup;

    const FLAG_NO_MAIL = 'no_mail';
    const FLAG_POST    = 'post';

    public function __construct()
    {
        $this->method    = $_SERVER['REQUEST_METHOD'];
        $this->remote_ip = $_SERVER['REMOTE_ADDR'];

        $this->logMsg('Deploying...');
    }

    public function deploy()
    {
        $commands = array(
            'cd '.$this->working_path.'; git fetch '.$this->remote.' 2>&1',
            'cd '.$this->working_path.'; git reset --hard 2>&1',
            'cd '.$this->working_path.'; git pull '.$this->remote.' '.$this->branch.' 2>&1'
        );

        foreach ($commands as $command) {
            exec($command, $output);
        }

        $this->logMsg(print_r($output, true));
        $this->mailMsg(print_r($output, true));

        return $this;
    }

    public function setup()
    {
        $this->setSetup()->setPayload()->setRepoData();

        return $this;
    }

    public function setRepoData()
    {
        // we fail in two cases
        if (count($this->payload['commits'])) {
            if ($this->setup['branch'] != $this->payload['commits'][0]['branch']) {
                // we fail quietly here
                $this->setError('Different branch ('.$this->payload['commits'][0]['branch'].')', self::FLAG_NO_MAIL);
            }
        }
        elseif (!isset($this->setup['default']) || isset($this->setup['default']) && $this->setup['default'] === false) {
            // we fail quietly here
            $this->setError('No commits and no default.', self::FLAG_NO_MAIL);
        }

        // if no fail we follow on
        $this->branch       = $this->setup['branch'];
        $this->remote       = $this->setup['remote'];
        $this->working_path = $this->setup['path'];
        $this->mail_to      = $this->setup['mail_to'];

        return $this;
    }

    public function setPayload()
    {
        if ($this->method != 'POST') {
            $this->setError('Not a POST request. '.$this->remote_ip.' has sent data via '.$this->method);
        }

        if (!isset($_POST['payload'])) {
            $this->setError('No payload in POST.');
        }

        $this->payload   = json_decode($_POST['payload'], true);
        $this->repo_name = $this->payload['repository']['name'];

        if (!isset($this->setup[$this->repo_name])) {
            $this->setError('No setup data for given repository: '.$this->repo_name);
        }

        $this->setup      = $this->setup[$this->repo_name];
        $this->allowed_ip = $this->setup['allowed_ip'];

        if (!empty($this->allowed_ip)) {
            if (!in_array($this->remote_ip, $this->allowed_ip)) {
                $this->setError('Remote ip ('.$this->remote_ip.') is not on the allowed list.');
            }
        }

        return $this;
    }

    public function setSetup()
    {
        if (!is_readable('./setup.json')) {
            $this->setError('setup.json not found or not readable');
        }

        $setup = json_decode(file_get_contents('./setup.json'), true);

        if (is_null($setup)) {
            $this->setError('Json Error: '.json_last_error_msg());
        }

        $this->setup      = $setup;
        $this->admin_mail = $setup['admin_mail'];

        return $this;
    }

    public function setError($msg, $flag = null)
    {
        $this->logMsg($msg);

        if ($flag != self::FLAG_NO_MAIL) {
            $this->mailMsg($msg);
        }

        if ($flag == self::FLAG_POST) {
            $this->logMsg($_POST);
        }

        // simple but effective
        die('Sorry, we encountered a problem.');
    }

    public function logMsg($msg)
    {
        $f = fopen(__DIR__.'/deploy.log', 'a');
        fwrite($f, date('Y-m-d H:i:s').': '.print_r($msg, true)."\n");
        fclose($f);
    }

    public function mailMsg($msg)
    {
        if (!empty($this->admin_mail)) {
            $title = 'Failure! Auto-deploy on '.$_SERVER['SERVER_ADDR'];

            // no test for commits as we fail without email in those cases
            if (!is_null($this->payload)) {
                $title = 'Success! Auto-deploy for repo: '.$this->repo_name.' branch: '.$this->payload['commits'][0]['branch'];
            }

            if (count($this->payload['commits'])) {
                $author_mail = preg_match('/<(.*?)>/i', $this->payload['commits'][0]['raw_author']);
                $headers     = 'BCC: '.$author_mail;
            }
            else {
                $headers = is_array($this->mail_to) ? 'BCC: '.implode(',', $this->mail_to) : null;
            }

            mail($this->admin_mail, $title, date('Y-m-d H:i:s').': '.$msg, $headers);
        }
    }
}