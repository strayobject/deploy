# Git Deploy script (for bitbucket)

Fairly simple, not necessarily perfect git deploy script for bitbucket based
 repositories.

## HOW TO

1. Create your setup.json file based on setup_.json.
2. Set a post hook on bitbucket.
3. Enjoy!

# TROUBLESHOOT (Ubuntu)

If you get key related errors it is most likely that your server user
(www-data/apache) does not have a deployment key set up. Here is how to do so:

1. Create a folder in /home
2. Change your server's home directory in `/etc/passwd` to the folder created
in step one.
3. Create a rsa key with `ssh-keygen -t rsa -b 4096`. Make sure you do not
set a passphrase.
4. Copy your ssh key and paste it into your bitbucket project's deplyment key
section.
5. Back on your server do an initial pull as your server user to add the
server to known hosts ie. `sudo -u www-data git pull origin master`
6. Try pushing something from your dev PC and see the log file if it was deployed.


## WARNING

As it stands ip limit and multpiple email notifications have not been tested yet.

I should not have to say this, but the code is provided as is, so be aware that
you should verify yourself if this code will not do any harm to your repo/server.

I will not be held liable for any damage caused by your or any third party use
of this code.